package poland.ididmychores;

import android.util.Log;

import com.google.gson.Gson;

public class LoginUserService extends AbstractConnect{

    public boolean verifyUser(String login, String password) {
        Log.d("Json", "login?login="+login+"&password="+password);
        String json = getJson("login?login="+login+"&password="+password);
        LoginResult fromJson = new Gson().fromJson(json, LoginResult.class);
        Log.d("Json", json);
        return fromJson.ok;
    }

    public boolean checkLoginAvaliability(String login) {
        String json = getJson("avaliability?login="+login);
        LoginResult fromJson = new Gson().fromJson(json, LoginResult.class);
        return fromJson.ok;
    }

    public boolean addUser(String login, String password) {
        String json = getJson("add?login="+login+"&password="+password);
        LoginResult fromJson = new Gson().fromJson(json, LoginResult.class);
        return fromJson.ok;
    }

    public static class LoginResult{
        boolean ok;
    }
}