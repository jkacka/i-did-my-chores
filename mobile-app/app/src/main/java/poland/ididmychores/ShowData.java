package poland.ididmychores;

/**
 * Created by asia on 16.09.17.
 */

public interface ShowData {

    void onTaskGenerated(TasksUserService.Tasks[] tasks);

    void onRankingGenerated(RankingUserServices.Ranking[] ranking);
}
