package poland.ididmychores;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class TaskActivity extends AppCompatActivity implements ShowData{
    String login = ""; // or other values
    String chore = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);

        LinearLayout tasklayout = (LinearLayout) findViewById(R.id.tasklayout);

        Bundle b = getIntent().getExtras();

        if (b != null) {
            login = b.getString("login");
            chore = b.getString("task");
            Log.d("JakiLogin", login);
        }

        tasklayout.addView(drawChore(getApplicationContext(), chore));

        LinearLayout buttonHolder = new LinearLayout(getApplicationContext());
        buttonHolder.setGravity(Gravity.CENTER);
        buttonHolder.setMinimumWidth(450);

        Button iDid = new Button(getApplicationContext());
        iDid.setText("Yes, i did");
        iDid.setTextColor(Color.WHITE);
        iDid.setBackgroundColor(Color.parseColor("#4CAF50"));
        iDid.setTextSize(26.0f);
        iDid.setMinimumWidth(450);


        iDid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SendTask st = new SendTask(TaskActivity.this);
                st.execute((Void) null);
            }
        });
        buttonHolder.addView(iDid);
        tasklayout.addView(buttonHolder);
        //setContentView(tasklayout);


    }

    private View drawChore(Context context, String chore) {
        ImageView choreImage = new ImageView(context);
        int id = this.getResources().getIdentifier(chore, "drawable",
                this.getApplicationContext().getPackageName());
        choreImage.setImageResource(id);
        choreImage.setLayoutParams(new LinearLayout.LayoutParams(500, 500));
        choreImage.setPadding(0,0,0,50);
        return choreImage;
    }

    @Override
    public void onTaskGenerated(TasksUserService.Tasks[] tasks) {
        finish();
    }

    @Override
    public void onRankingGenerated(RankingUserServices.Ranking[] ranking) {

    }

    public class SendTask extends AsyncTask<Object, Object, Object> {

        TasksUserService.Tasks[] tasksArray;

        public ShowData delegate = null;//Call back interface

        public SendTask(ShowData showTasks) {
            delegate = showTasks;//Assigning call back interfacethrough constructor
        }

        @Override
        protected Object doInBackground(Object... params) {
            TasksUserService tasksUserService = new TasksUserService();
            tasksUserService.sendTask(login, chore);
            return "OK";
        }

        @Override
        protected void onPostExecute(Object result) {
            finish();
        }

        @Override
        protected void onCancelled() {
            ;
        }
    }
}