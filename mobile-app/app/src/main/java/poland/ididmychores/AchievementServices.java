package poland.ididmychores;

import com.google.gson.Gson;

public class AchievementServices extends AbstractConnect{

    private Achievemnts[] achivements;

    public Achievemnts[] getAchivements(String login) {
        Gson gson = new Gson();
        String json = getJson("achievements?login="+login);
        achivements = gson.fromJson(json, Achievemnts[].class);

        return achivements;
    }

    public static class Achievemnts{
        String taskName;
        Integer quantity;

        public String toString(){
            return taskName + " " + quantity;
        }
    }


}