package poland.ididmychores;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import java.util.Arrays;
import java.util.List;

public class TasksUserService extends AbstractConnect{

    private Tasks[] tasksArray;

    public Tasks[] gestTasks() {
        Gson gson = new Gson();
        String json = getJson("tasks");
        tasksArray = gson.fromJson(json, TasksUserService.Tasks[].class);

        return tasksArray;

    }


    public static class Tasks{
        String taskName;
        Integer points;
        String dateTime;
        Integer frequency;

        public String toString(){
            return taskName + " " + points + " "  + dateTime + " " + frequency;
        }
    }
    public void sendTask(String login, String task){
        String json = getJson("taskupdate?login="+login+"&taskName="+task);

    }

}