package poland.ididmychores;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class AwardActivity extends AppCompatActivity implements ShowAchv{

    private String login;
    private String achievementName;//do pobrania z bazy
    private int number;

    public void onAchvGenerated(AchievementServices.Achievemnts[] achvList){
        //main layout
        RelativeLayout relLayout = (RelativeLayout) findViewById(R.id.activityAward);

        LinearLayout achievmentsLayout = new LinearLayout(getApplicationContext());
        achievmentsLayout.setOrientation(LinearLayout.VERTICAL);


        for(AchievementServices.Achievemnts achv: achvList) {
            Log.d("achv", achv.taskName);
            LinearLayout achievmentBox = new LinearLayout(getApplicationContext());
            achievmentBox.setOrientation(LinearLayout.HORIZONTAL);
            achievmentBox.addView(drawAchievement(getApplicationContext(), achv.taskName.toLowerCase(), achv.quantity));
            achievmentBox.addView(createDescription(achv.taskName.toLowerCase(), achv.quantity));
            achievmentsLayout.addView(achievmentBox);
        }
        relLayout.addView(achievmentsLayout);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_award);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            login = b.getString("login");
            Log.d("JakiLogin", login);
        }

        AchvTask at = new AchvTask(AwardActivity.this);
        at.execute((Void) null);

    }

    private TextView createDescription(String achievementName, int number) {
        String achievementText = "";
        Log.d("kala",achievementName);
        if(achievementName.equals("dishes"))
            achievementText="You have done the dishes at least 3x!";
        else if(achievementName.equals("garbage"))
            achievementText="You have taken out the garbage at least 3x!";
        else if(achievementName.equals("laundry"))
            achievementText="You have done the laundry at least 3x!";
        else if(achievementName.equals("wc"))
            achievementText="You have cleaned toilet at least 3x!";

        int color = 0;
        if(number>=3)
            color=Color.BLUE;
        else
            color=Color.GRAY;

        TextView achievementDescription = new TextView(getApplicationContext());
        achievementDescription.setText(achievementText);
        achievementDescription.setTextSize(20.0f);
        achievementDescription.setTextColor(color);
        achievementDescription.setGravity(Gravity.CENTER);
        return achievementDescription;
    }

    private View drawAchievement(Context context, String achievementName, int number) {
        ImageView achievemetImage = new ImageView(context);
        String imageName = "";
        if(number>=3)
            imageName=achievementName+"3";
        else
            imageName=achievementName+"3g";

        int id = this.getResources().getIdentifier(imageName, "drawable",
                this.getApplicationContext().getPackageName());
        achievemetImage.setImageResource(id);
        achievemetImage.setLayoutParams(new LinearLayout.LayoutParams(200,200));
        return achievemetImage;
    }


    public class AchvTask extends AsyncTask<Object, Object, Object> {

        private AchievementServices.Achievemnts[] achvList;

        public ShowAchv delegate = null;//Call back interface

        public AchvTask(ShowAchv showAchv) {
            delegate = showAchv;//Assigning call back interfacethrough constructor
        }

        @Override
        protected Object doInBackground(Object... params){
            AchievementServices achievementServices = new AchievementServices();
            achvList =  achievementServices.getAchivements(login);
            return achvList;
        }

        @Override
        protected void onPostExecute(Object result) {
            AchievementServices.Achievemnts[] achvList = (AchievementServices.Achievemnts[])result;
            delegate.onAchvGenerated(achvList);
        }

        @Override
        protected void onCancelled() {
            ;
        }
    }
}
