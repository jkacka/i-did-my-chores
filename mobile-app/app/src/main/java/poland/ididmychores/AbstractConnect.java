package poland.ididmychores;

import android.util.Log;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

public abstract class AbstractConnect implements Serializable {

    private static String IP_ADDRESS = "172.30.5.139:8080";

    public static void setIPAddress(String address){
        IP_ADDRESS = address;
    }

    protected String getJson(String path) {
        try {
            Scanner scanner = connectToUrl(path);
            return getAllText(scanner);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private Scanner connectToUrl(String path) throws IOException,
            MalformedURLException {
        URLConnection connection = new URL(buildUrl(path)).openConnection();
        Scanner scanner = new Scanner(connection.getInputStream(), "ISO-8859-1");
        return scanner;
    }

    private String buildUrl(String path) {
        Log.d("Json", "http://" + IP_ADDRESS  +"/" + path);
        return "http://" + IP_ADDRESS  +"/" + path;
    }

    private String getAllText(Scanner scanner) {
        String json = "";
        while (scanner.hasNext()) {
            json += scanner.next();
        }
        Log.i("json", json);
        return json;
    }
}