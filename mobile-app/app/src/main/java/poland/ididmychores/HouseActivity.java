package poland.ididmychores;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class HouseActivity extends AppCompatActivity implements ShowData {

    private LinearLayout leftlayout;
    private LinearLayout rightlayout;
    private String login;

    public void onTaskGenerated(TasksUserService.Tasks[] tasksArray){

        String listOfTasks ="";
        for(TasksUserService.Tasks t: tasksArray) {
            Log.d("Task: ", t.toString());
            listOfTasks += "TASK: " + t.toString() + ";";

            LinearLayout choreLayout = new LinearLayout(getApplicationContext());
            choreLayout.setOrientation(LinearLayout.HORIZONTAL);
            choreLayout.addView(drawChore(getApplicationContext(), t.taskName.toLowerCase()));
            choreLayout.addView(createChoreInfoLayout(t.dateTime, t.points));
            leftlayout.addView(choreLayout);
        }
        Log.d("Tasks", listOfTasks);

    }

    public void onRankingGenerated(RankingUserServices.Ranking[] ranking){

        String listOfRanking ="";
        Integer i=1;

        LinearLayout textLayout = new LinearLayout(getApplicationContext());
        TextView informationView = new TextView(this);
        informationView.setText("RANKING:");
        informationView.setTextSize(25);
        informationView.setTextColor(Color.BLACK);
        textLayout.addView(informationView);
        rightlayout.addView(textLayout);



        for(RankingUserServices.Ranking r: ranking) {
            Log.d("User: ", r.toString());
            listOfRanking += "RANK: " + r.toString() + ";";

            textLayout = new LinearLayout(getApplicationContext());

            informationView = new TextView(this);
            informationView.setText(i+". " + r.toString());
            informationView.setTextSize(15);
            informationView.setTextColor(Color.BLACK);

            textLayout.addView(informationView);

            rightlayout.addView(textLayout);

            i++;
        }
        Log.d("Tasks", listOfRanking);

        LinearLayout awardLayout = (LinearLayout) findViewById(R.id.awardLayout);

        ImageView awardImage = new ImageView(getApplicationContext());
        int id = this.getResources().getIdentifier("goodjob", "drawable",
                this.getApplicationContext().getPackageName());
        awardImage.setImageResource(id);
        awardImage.setLayoutParams(new LinearLayout.LayoutParams(250,250));
        awardImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent awardIntent = new Intent(HouseActivity.this, AwardActivity.class);
                Bundle b = new Bundle();
                Log.d("JakiLogin", login);
                b.putString("login", login);
                awardIntent.putExtras(b); //Optional parameters
                startActivity(awardIntent);
            }
        });
        awardLayout.addView(awardImage);


    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_house);

        Bundle b = getIntent().getExtras();
        login = ""; // or other values
        if(b != null)
            login = b.getString("login");

        Log.d("LoginHA", login);

        leftlayout = (LinearLayout) findViewById(R.id.leftlayout);
        rightlayout = (LinearLayout) findViewById(R.id.rightlayout);

        //TaskTask tt = new TaskTask(HouseActivity.this);
        //tt.execute((Void) null);

        //RankingTask rt = new RankingTask(HouseActivity.this);
        //rt.execute((Void) null);

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Log.d("LoginResume", login);

        leftlayout.removeAllViews();
        rightlayout.removeAllViews();

        LinearLayout awardLayout = (LinearLayout) findViewById(R.id.awardLayout);
        awardLayout.removeAllViews();

        TaskTask tt = new TaskTask(HouseActivity.this);
        tt.execute((Void) null);

        RankingTask rt = new RankingTask(HouseActivity.this);
        rt.execute((Void) null);

    }

    private LinearLayout createChoreInfoLayout(String lastDone, Integer points) {
        LinearLayout choreInfo = new LinearLayout(getApplicationContext());
        choreInfo.setOrientation(LinearLayout.VERTICAL);
        TextView lastDoneInfo = new TextView(getApplicationContext());
        lastDoneInfo.setText("Last done: " + lastDone);
        lastDoneInfo.setTextColor(Color.BLACK);
        TextView pointsInfo = new TextView(getApplicationContext());
        pointsInfo.setText("Points: " + points);
        pointsInfo.setTextColor(Color.BLACK);

        choreInfo.addView(lastDoneInfo);
        choreInfo.addView(pointsInfo);
        return choreInfo;
    }

    private View drawChore(Context context, final String chore) {
        ImageView choreImage = new ImageView(context);
        int id = this.getResources().getIdentifier(chore, "drawable",
                this.getApplicationContext().getPackageName());
        choreImage.setImageResource(id);
        choreImage.setLayoutParams(new LinearLayout.LayoutParams(200,200));
        choreImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(HouseActivity.this, TaskActivity.class);
                Bundle b = new Bundle();
                Log.d("JakiLogin", login);
                b.putString("task", chore);
                b.putString("login", login);
                myIntent.putExtras(b); //Optional parameters
                startActivity(myIntent);
            }
        });
        return choreImage;
    }


    public class TaskTask extends AsyncTask<Object, Object, Object> {

        TasksUserService.Tasks[] tasksArray;

        public ShowData delegate = null;//Call back interface

        public TaskTask(ShowData showTasks) {
            delegate = showTasks;//Assigning call back interfacethrough constructor
        }

        @Override
        protected Object doInBackground(Object... params){
            TasksUserService tasksUserService = new TasksUserService();
            tasksArray =  tasksUserService.gestTasks();
            return tasksArray;
        }

        @Override
        protected void onPostExecute(Object result) {
            TasksUserService.Tasks[] array = (TasksUserService.Tasks[])result;
            delegate.onTaskGenerated(array);
        }

        @Override
        protected void onCancelled() {
            ;
        }
    }

    public class RankingTask extends AsyncTask<Object, Object, Object> {

        private RankingUserServices.Ranking[] rankingUsers;;

        public ShowData delegate = null;//Call back interface

        public RankingTask(ShowData showData) {
            delegate = showData;//Assigning call back interfacethrough constructor
        }

        @Override
        protected Object doInBackground(Object... params){
            RankingUserServices rankingUserService = new RankingUserServices();
            rankingUsers =  rankingUserService.getRankingUsers();
            return rankingUsers;
        }

        @Override
        protected void onPostExecute(Object result) {
            RankingUserServices.Ranking[] array = (RankingUserServices.Ranking[])result;
            delegate.onRankingGenerated(array);
        }

        @Override
        protected void onCancelled() {
            ;
        }
    }

}
