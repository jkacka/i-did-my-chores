package poland.ididmychores;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import java.util.Arrays;
import java.util.List;

public class RankingUserServices extends AbstractConnect{

    private Ranking[] rankingUsers;

    public Ranking[] getRankingUsers() {
        Gson gson = new Gson();
        String json = getJson("rankingwhole");
        rankingUsers = gson.fromJson(json, Ranking[].class);

        return rankingUsers;

    }


    public static class Ranking{
        String login;
        Boolean ok;
        Integer points;


        public String toString(){
            return login + " " + points;
        }
    }


}